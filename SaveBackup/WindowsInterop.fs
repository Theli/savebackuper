﻿module WindowsInterop

open System
open System.Windows.Forms


let dialog description = 
    let windowsDialog = new System.Windows.Forms.FolderBrowserDialog()
    windowsDialog.Description <- description
    windowsDialog.ShowNewFolderButton <- false
    //windowsDialog.RootFolder <- Environment.SpecialFolder.MyComputer
    async {
        match windowsDialog.ShowDialog() with
        | DialogResult.OK -> 
            return Some windowsDialog.SelectedPath
        | _ -> return None
    }