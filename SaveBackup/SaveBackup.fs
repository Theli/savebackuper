﻿// Copyright 2018-2019 Fabulous contributors. See LICENSE.md for license.
namespace SaveBackup

open System.Diagnostics
open Fabulous
open Fabulous.XamarinForms
open Xamarin.Forms

[<AutoOpen>]
module Extensions = 
    module Async = 
        let map f computation = async {
            let! result = computation
            return f result
        }
    open System
    type DateTime with 
         static member UnixTimeNow = int64 (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
            

    [<AutoOpen>]
    module Path = 
        
        let (</>) root relative = System.IO.Path.Combine(root, relative)
        let sanitizeDirectoryName (name: string) = 
            let invalids = System.IO.Path.GetInvalidPathChars()
            let sn = System.String.Join("_", name.Split(invalids, StringSplitOptions.RemoveEmptyEntries) ).TrimEnd('.')
            if sn.Length > 6 then sn.Substring(0, 6) else sn

    module FileSystem = 
        open System.IO
        let copyFolder sourceAbsolutePath destinationAbsolutePath = 
            let rec copyRecursively (source: DirectoryInfo) (destination: DirectoryInfo) = 
                for sourceSubDir in source.GetDirectories() do
                    copyRecursively sourceSubDir (destination.CreateSubdirectory(sourceSubDir.Name))
                for sourceFile in source.GetFiles() do
                    sourceFile.CopyTo(destination.FullName </> sourceFile.Name) |> ignore
            
            if Directory.Exists(destinationAbsolutePath) |> not
                then Directory.CreateDirectory destinationAbsolutePath |> ignore
            let sourceInfo = DirectoryInfo sourceAbsolutePath
            let destinationInfo = DirectoryInfo destinationAbsolutePath
            if sourceInfo.Exists then
                copyRecursively sourceInfo destinationInfo

        let unsafeDeleteFolder folderPath = 
            if Directory.Exists folderPath then
                Directory.Delete(folderPath, true)
                while (Directory.Exists(folderPath)) do System.Threading.Thread.Sleep(0);

        let unsafeDeleteFolderContents folderPath = 
            for file in Directory.EnumerateFiles folderPath do
                File.Delete file
            for subDir in Directory.EnumerateDirectories folderPath do
                Directory.Delete(subDir, true)


module Example = 
    type AnotherOne = { Data: byte []}
    type League = {
        CountryName: string
        SecondOne: int
        Prop1: float
        Prop2: float
        Prop3: bool
        Prop4: float
        Prop5: float
        Prop6: Option<AnotherOne>
        Prop7: float
        Prop8: float
        Prop9: float
    }
    type HugeFuckingObjectWithLoadsOfProperties = {
        IsFrozen: bool
        League: League
    }
    
    let (|CanceledDueToFreezing|LeagueMissingProp6|UkraineIsBanned|TooMuchData|Valid|) obj = 
        match obj with
        | { IsFrozen = true } -> CanceledDueToFreezing
        | { League = {Prop6 = None} } -> LeagueMissingProp6
        | { League = {CountryName = "Ukraine" } } -> UkraineIsBanned
        | { League = {Prop6 = Some anotherOne } } when anotherOne.Data |> Array.length > 100 -> TooMuchData
        | _ -> Valid obj
                
            

module App = 
    open System
    let backuperAppPath = System.AppDomain.CurrentDomain.BaseDirectory
    let backuperAppDataPath = backuperAppPath </> "Backups" 
    let tempDataPath = backuperAppPath </> "Temp"
    let random = System.Random()


    type BackedFolderState = 
        {
            SaveDate: DateTime
            AdditionalNotes: string
            GeneratedFolderName: string
        }
        static member Create(name: string) = 
            let time = DateTime.Now
            let unixTimestamp = DateTime.UnixTimeNow
            let sanitizedName = sanitizeDirectoryName name
            let randomNumber = random.Next()
            let generatedFolderRelativeName = sprintf "%s_%i_%i" sanitizedName unixTimestamp randomNumber
            let generatedFolderName = backuperAppDataPath </> generatedFolderRelativeName
            {SaveDate = time; GeneratedFolderName = generatedFolderName; AdditionalNotes = ""}

            

    type BackedFolder = 
        {
            SourcePath: string
            Name: string
            BackupRecords: BackedFolderState list
            SelectedEntryIndex: int option
        }
        static member Create(sourcePath: string, name: string) = 
            {SourcePath = sourcePath; Name = name; BackupRecords = []; SelectedEntryIndex = None}

    type SaveBackupApp = 
        {
            BackedFolders: BackedFolder list
            SelectedBackedFolderIndex: int option
        }
        member x.CurrentlySelectedFolder = 
            match x.SelectedBackedFolderIndex with
            | Some i when i < x.BackedFolders.Length -> Some x.BackedFolders.[i]
            | _ -> None

    type BackedFolderMessage = 
        | SaveCurrentFolder
        | RestoreFolderToSpecificState of BackedFolderState
        | DeleteBackup of BackedFolderState
        | SelectBackup of int option
    

        

    type SaveBackupMsg = 
        | OpenFolderBrowser
        | OpenFolderByPath of string
        | SelectBackedUpFolder of index: int
        | FolderMessage of BackedFolderMessage
        | DeselectFolder

    open MBrace.FsPickler
    open System.IO


    let binarySerializer = FsPickler.CreateBinarySerializer()

    [<Literal>]
    let databaseFileName = "backup.db"
    let databaseFilePath = backuperAppPath </> databaseFileName

    let extractAppName (path: string) = 
        let parts = path.Split(Path.DirectorySeparatorChar)
        parts 
        |> Array.skipWhile (fun part -> 
            let lower = part.ToLower()
            lower.Contains("c:") 
            || lower.Contains("d:") 
            || lower.Contains("appdata") 
            || lower.Contains ("steamapp") 
            || lower.Contains("program files")
            || lower.Contains("users")
            || lower = Environment.UserName.ToLower())
        |> Array.tryHead
        |> Option.defaultWith (fun () -> sprintf "Unnamed_%i" (random.Next()))


    let init () = 
        
        if File.Exists databaseFilePath then
            let bytes = File.ReadAllBytes databaseFilePath
            binarySerializer.UnPickle<SaveBackupApp> bytes, Cmd.none
        else { BackedFolders = []; SelectedBackedFolderIndex = None }, Cmd.none


    let saveState (appState: SaveBackupApp) = 
        let bytes = binarySerializer.Pickle appState
        File.WriteAllBytes(databaseFilePath, bytes)
            

    let updateFolder (currentFolder: BackedFolder) message = 
        match message with
        | SaveCurrentFolder -> 
            let backupRecord = BackedFolderState.Create(currentFolder.Name)
            FileSystem.copyFolder currentFolder.SourcePath backupRecord.GeneratedFolderName
            { currentFolder with BackupRecords = backupRecord::currentFolder.BackupRecords }
        | RestoreFolderToSpecificState backupRecord -> 
            let timestamp = DateTime.UnixTimeNow
            let sanitizedName = sanitizeDirectoryName currentFolder.Name
            let tempFolderPath = tempDataPath </> (sprintf "%s_%i" sanitizedName timestamp)
            FileSystem.copyFolder currentFolder.SourcePath tempFolderPath
            FileSystem.unsafeDeleteFolder currentFolder.SourcePath
            FileSystem.copyFolder backupRecord.GeneratedFolderName currentFolder.SourcePath
            currentFolder
        | DeleteBackup backupRecord -> 
            FileSystem.unsafeDeleteFolder backupRecord.GeneratedFolderName
            { currentFolder with BackupRecords = currentFolder.BackupRecords |> List.filter (fun r -> r = backupRecord); SelectedEntryIndex = None}
        | SelectBackup indexOption -> 
            { currentFolder with SelectedEntryIndex = indexOption }

    let update  message state = 
        match message with
        | OpenFolderBrowser -> 
            let dialogResult = async {
                
                let! selection = 
                    WindowsInterop.dialog "Select folder to backup"
                match selection  with
                | Some path -> return OpenFolderByPath path
                | _ -> return DeselectFolder
            }
            state, Cmd.ofAsyncMsg dialogResult
        | OpenFolderByPath path -> 
            let updatedFoldersList, index = 
                let existing = 
                    state.BackedFolders 
                    |> List.indexed
                    |> List.tryFind (fun (i, bf) -> bf.SourcePath = path)
                match existing with
                | Some(index, _) -> state.BackedFolders, Some index
                | _ -> 
                    let name = extractAppName path
                    let newFolder = BackedFolder.Create(path, name)
                    newFolder::state.BackedFolders, Some 0
            {state with BackedFolders = updatedFoldersList; SelectedBackedFolderIndex = index}, Cmd.none

        | SelectBackedUpFolder index when state.BackedFolders.Length > index -> {state with SelectedBackedFolderIndex = Some index}, Cmd.none
        | FolderMessage msg' when state.SelectedBackedFolderIndex.IsSome && state.BackedFolders.Length > state.SelectedBackedFolderIndex.Value ->
            let currentFolder = state.BackedFolders |> List.item state.SelectedBackedFolderIndex.Value
            let updatedFolder = updateFolder currentFolder msg'
            {state with BackedFolders = state.BackedFolders |> List.map (fun bf -> if bf = currentFolder then updatedFolder else bf)}, Cmd.none
        | DeselectFolder -> {state with SelectedBackedFolderIndex = None}, Cmd.none

        | _ -> failwith "Unexpected program state"
        |> fun (updatedState, cmd) -> 
            if updatedState.BackedFolders <> state.BackedFolders then saveState updatedState
            updatedState, cmd
    

    let listOfBackedUpFoldersAndBrowseView (backedFolders: BackedFolder list) dispatch = 
        let labels = backedFolders |> List.map (fun bf -> 
            View.Label(text = bf.Name, horizontalOptions = LayoutOptions.Center, widthRequest=300.0, horizontalTextAlignment=TextAlignment.Center))
        let list = 
            View.ListView(
                items = labels,
                itemSelected = function | Some newIndex -> newIndex |> SelectBackedUpFolder |> dispatch | _ -> dispatch DeselectFolder )
        let browseButton = View.Button(text = "Browse", horizontalOptions = LayoutOptions.Center, command = (fun () -> dispatch OpenFolderBrowser))
        View.ContentPage(
          content = View.StackLayout(padding = 20.0, verticalOptions = LayoutOptions.Center,
            children = [ 
                list
                browseButton
            ]))

    let backedFolderView (currentFolder: BackedFolder) dispatch = 
        let deselectButton = View.Button(text = "Back", command = (fun () -> dispatch DeselectFolder), horizontalOptions = LayoutOptions.Start)
        let backupsListItems = 
            currentFolder.BackupRecords 
            |> List.map (fun br -> 
                View.Label(
                    text = sprintf "%s" (br.SaveDate.ToString("HH:mm dd-MM-yy")), 
                    horizontalOptions = LayoutOptions.Center, 
                    widthRequest=300.0, 
                    horizontalTextAlignment=TextAlignment.Center))
        let backupListView = 
            View.ListView(
                items = backupsListItems,
                itemSelected = fun newIndex -> newIndex |> SelectBackup |> FolderMessage |> dispatch )
        let saveButton = View.Button(text = "Save folder", horizontalOptions = LayoutOptions.Center, command = (fun () -> SaveCurrentFolder |> FolderMessage |> dispatch))
        let backupsList = 
            View.StackLayout(padding = 20.0, horizontalOptions = LayoutOptions.Center,
                children = [ 
                    backupListView
                    View.Button(text = "Restore", 
                        horizontalOptions = LayoutOptions.Center, 
                        command = (fun () -> currentFolder.SelectedEntryIndex |> Option.iter(fun i -> currentFolder.BackupRecords.[i] |> RestoreFolderToSpecificState |> FolderMessage |> dispatch)), canExecute = currentFolder.SelectedEntryIndex.IsSome)
                ])
            
        View.ContentPage(
          content = View.StackLayout(padding = 20.0, verticalOptions = LayoutOptions.Center,
            children = [ 
                deselectButton
                saveButton
                backupsList
            ]))

    let view (state: SaveBackupApp) dispatch = 
        match state.CurrentlySelectedFolder with
        | Some folder -> backedFolderView folder dispatch
        | _ -> listOfBackedUpFoldersAndBrowseView state.BackedFolders dispatch

        

    

    // Note, this declaration is needed if you enable LiveUpdate
    let program = Program.mkProgram init update view

type App () as app = 
    inherit Application ()

    let runner = 
        App.program
#if DEBUG
        |> Program.withConsoleTrace
#endif
        |> XamarinFormsProgram.run app

#if DEBUG
    // Uncomment this line to enable live update in debug mode. 
    // See https://fsprojects.github.io/Fabulous/tools.html for further  instructions.
    //
    //do runner.EnableLiveUpdate()
#endif    

    // Uncomment this code to save the application state to app.Properties using Newtonsoft.Json
    // See https://fsprojects.github.io/Fabulous/models.html for further  instructions.
#if APPSAVE
    let modelId = "model"
    override __.OnSleep() = 

        let json = Newtonsoft.Json.JsonConvert.SerializeObject(runner.CurrentModel)
        Console.WriteLine("OnSleep: saving model into app.Properties, json = {0}", json)

        app.Properties.[modelId] <- json

    override __.OnResume() = 
        Console.WriteLine "OnResume: checking for model in app.Properties"
        try 
            match app.Properties.TryGetValue modelId with
            | true, (:? string as json) -> 

                Console.WriteLine("OnResume: restoring model from app.Properties, json = {0}", json)
                let model = Newtonsoft.Json.JsonConvert.DeserializeObject<App.Model>(json)

                Console.WriteLine("OnResume: restoring model from app.Properties, model = {0}", (sprintf "%0A" model))
                runner.SetCurrentModel (model, Cmd.none)

            | _ -> ()
        with ex -> 
            App.program.onError("Error while restoring model found in app.Properties", ex)

    override this.OnStart() = 
        Console.WriteLine "OnStart: using same logic as OnResume()"
        this.OnResume()
#endif


